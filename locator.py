from selenium.webdriver.common.by import By

class MainPageLocators(object):
    PRODUCT = (By.XPATH, "//*[@id='homefeatured']/li/div/div/div/a/img")
    PRODUCT_LINK = (By.XPATH, "*[text()='Add to cart']")
    
class ProductPageLocators(object):
    ADD_TO_CART_BUTTON = (By.XPATH, "//span[text()='Add to cart']")
    X_BUTTON = (By.XPATH, "//span[@class='cross']")
    PURCHASE_STATUS = (By.XPATH, "//*[@id='layer_cart']/div/div/h2")
    
class CartPageLocators(object):
    DELETE_BUTTON = (By.XPATH, "//a[@class='cart_quantity_delete']")
    SUCCESS_TEXT = (By.XPATH, "//*[@id='layer_cart']/div/div/h2")
    PRODUCT_DELETED_TEXT = (By.XPATH, "//p[@class='alert alert-warning']")
    
class SearchLocators(object):
    SEARCH_BUTTON = (By.XPATH, "//button[@name='submit_search']")
    SEARCH_RESULTS = (By.XPATH, "//span[@class='heading-counter']")
    
class StoreInformationLocators(object):
    FOOTER_INFO = (By.XPATH, "//section[@id='block_contact_infos']")