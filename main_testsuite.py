import unittest
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
import page
import time
import requests
import json

class MyStoreAutomation(unittest.TestCase):
    
    #SETUP before tests
    def setUp(self):
        self.driver = webdriver.Chrome(ChromeDriverManager().install())
        self.driver.maximize_window()
        self.driver.get("http://automationpractice.com/index.php")
        
    #initialize the class with the method you want to try, then assert the method
    def test_Add_Cart(self):
        print("Testing adding to cart...")
        self.driver.get("http://automationpractice.com/index.php?id_product=1&controller=product")
        productPage = page.ProductPage(self.driver)
        productPage.add_to_cart()
        time.sleep(1.5)
        try:
            assert productPage.is_add_to_cart()
            print("Test passed!")
        except AssertionError:
            print("Add to cart test assertion failed.")
    
    def test_Delete_Item(self):
        print("Testing deleting item from cart...")
        self.driver.get("http://automationpractice.com/index.php?id_product=1&controller=product")
        productPage = page.ProductPage(self.driver)
        productPage.add_to_cart()
        time.sleep(1)
        self.driver.get("http://automationpractice.com/index.php?controller=order")
        shoppingCart = page.ShoppingCartPage(self.driver)
        shoppingCart.click_delete_button()
        time.sleep(1.5)
        try:
            assert shoppingCart.is_product_deleted()
            print("Test passed!")
        except AssertionError:
            print("Delete test assertion failed.")
    
    def test_Search(self):
        print("Testing searching...")
        mainPage = page.MainPage(self.driver)
        mainPage.search_text_element = "blouse"
        mainPage.click_search_button()
        search_result_page = page.SearchResultPage(self.driver)
        try:
            assert search_result_page.is_results_found()
            print("Test passed!")
        except AssertionError:
            print("Search test assertion failed.")
    
    def test_Footer_Info(self):
        print("Validating footer info...")
        mainPage = page.MainPage(self.driver)
        assert mainPage.is_footer_info_valid()
        print("Footer info validated!")
    
    #After all tests are done, close everything
    def tearDown(self):
        self.driver.close()


class BreakingApis(unittest.TestCase):
    
    #i've never watched this show
    
    def test_Breaking_Birthday(self):
        r = requests.get("https://breakingbadapi.com/api/characters?name=Walter+White")
        resp = json.loads(r.text)
        print(resp[0]['birthday'])
    
    def test_Character_Info(self):
        r = requests.get("https://breakingbadapi.com/api/characters")
        resp = json.loads(r.text) #json in dictionary form
        cast=""
        for attribute in resp:
            cast += 'Character name: "{0}"\n'.format(attribute['name'])
            cast += 'Portrayed: "{0}"\n'.format(attribute['portrayed'])
            cast += '------------------------------------------------------\n'
        print(cast)

if __name__ == '__main__':
    #run every test in main, aka test suite
    unittest.main()
    pass