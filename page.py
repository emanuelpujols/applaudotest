from locator import *
from element import BasePageElement

class SearchTextElement(BasePageElement):
    locator = "//input[@id='search_query_top']"

class BasePage(object):
    
    def __init__(self, driver):
        self.driver = driver
        
class MainPage(BasePage):
    
    search_text_element = SearchTextElement()
    
    def click_search_button(self):
        element = self.driver.find_element(*SearchLocators.SEARCH_BUTTON)
        element.click()
        
    def click_product(self):
        element = self.driver.find_element(*MainPageLocators.PRODUCT)
        element.click()
        
    def click_product_link(self):
        element = self.driver.find_element(*MainPageLocators.PRODUCT_LINK)
        element.click()
    
    def is_footer_info_valid(self):
        FOOTER_INFO = """Store information
Selenium Framework, Research Triangle Park, North Carolina, USA
Call us now: (347) 466-7432
Email: support@seleniumframework.com"""
        return(FOOTER_INFO == self.driver.find_element(*StoreInformationLocators.FOOTER_INFO).text)
        

class SearchResultPage(BasePage):
    def is_results_found(self):
        SEARCH_RESULT = "1 result has been found."
        FALSE_RESULT = "0 results have been found."
        return(SEARCH_RESULT == self.driver.find_element(*SearchLocators.SEARCH_RESULTS).text and FALSE_RESULT != self.driver.find_element(*SearchLocators.SEARCH_RESULTS).text)

class ProductPage(BasePage):
    def add_to_cart(self):
        element = self.driver.find_element(*ProductPageLocators.ADD_TO_CART_BUTTON) #the * will separate the tuple into two args
        element.click()
        
    def is_add_to_cart(self):
        SUCCESS_TEXT = "Product successfully added to your shopping cart"
        return(SUCCESS_TEXT == self.driver.find_element(*CartPageLocators.SUCCESS_TEXT).text)
    
class ShoppingCartPage(BasePage):
    def click_delete_button(self):
        element = self.driver.find_element(*CartPageLocators.DELETE_BUTTON)
        element.click()

    def is_product_deleted(self):
        EMPTY_CART_TEXT = "Your shopping cart is empty."
        return(EMPTY_CART_TEXT == self.driver.find_element(*CartPageLocators.PRODUCT_DELETED_TEXT).text)